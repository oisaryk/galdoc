import { Component, OnInit } from '@angular/core';
import { PrintBaseComponent } from '../../shared/components/print-base/print-base.component';
import { FormBuilder } from '@angular/forms';
import { DateService } from '../../shared/services/date.service';
import { PrintDataService } from '../../shared/services/print-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-print-layout',
  templateUrl: './print-layout.component.html',
  styleUrls: ['./print-layout.component.scss']
})
export class PrintLayoutComponent extends PrintBaseComponent implements OnInit {

  constructor(
    fb: FormBuilder,
    date: DateService,
    data: PrintDataService,
    router: Router
  ) {
    super(fb, date, data, router);
    super.ngOnInit();
  }

  ngOnInit(): void {
  }

}
