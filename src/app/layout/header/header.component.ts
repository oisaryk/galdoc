import { Component, OnInit } from '@angular/core';

export interface HeaderLinks {
  route: string;
  title: string;
}

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  links: HeaderLinks[] = [
    {
      route: '/blank/uzd-organiv-malogo-tazu',
      title: 'УЗД ОМТ'
    },
    {
      route: '/blank/uzd-pregnancy',
      title: 'УЗД вагітності'
    },
    {
      route: '/blank/uzd-molochnuh-zaloz',
      title: 'УЗД МЗ'
    },
    {
      route: '/blank/uzd-protocol',
      title: 'УЗД протокол'
    },
    {
      route: '/blank/uzd-tservikometria',
      title: 'Цервікометрія'
    },
    {
      route: '/blank/uzd-doplerometry',
      title: 'Доплерометрія'
    },
    {
      route: '/blank/uzd-two-three-uz',
      title: 'II-III УЗ'
    },
    {
      route: '/blank/uzd-protocol-two-uz',
      title: 'II УЗ'
    },
    {
      route: '/blank/uzd-protocol-one-uz',
      title: 'I УЗ'
    },
    {
      route: '/blank/uzd-organiv-cherevnoi',
      title: 'УЗ Черевної'
    },
    {
      route: '/blank/uzd-shitovidna',
      title: 'УЗ Щитовидної'
    },
    {
      route: '/blank/uzd-stateva',
      title: 'УЗ Статевої'
    }
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
