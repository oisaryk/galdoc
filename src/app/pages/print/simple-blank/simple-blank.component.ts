import { Component, Input, OnInit } from '@angular/core';
import { PrintBaseComponent } from '../../../shared/components/print-base/print-base.component';
import { FormBuilder } from '@angular/forms';
import { DateService } from '../../../shared/services/date.service';
import { PrintDataService } from '../../../shared/services/print-data.service';
import { Router } from '@angular/router';
import { isEmpty } from 'lodash';

@Component({
  selector: 'app-simple-blank',
  templateUrl: './simple-blank.component.html',
  styleUrls: ['./simple-blank.component.scss']
})
export class SimpleBlankComponent extends PrintBaseComponent implements OnInit {

  @Input() fontSize;
  @Input() lineHeight;
  @Input() borderValue;
  @Input() strongTitles;
  @Input() showEmptyRows;

  constructor(
    fb: FormBuilder,
    date: DateService,
    data: PrintDataService,
    router: Router
  ) {
    super(fb, date, data, router);
    super.ngOnInit();
  }

  ngOnInit(): void {

  }

  showEmptyRow(field) {

    if (this.showEmptyRows) {
      return true;
    }

    if (!field) {
      return false;
    }

    return !isEmpty(field);

  }

}
