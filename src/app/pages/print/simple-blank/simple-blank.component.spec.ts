import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimpleBlankComponent } from './simple-blank.component';

describe('SimpleBlankComponent', () => {
  let component: SimpleBlankComponent;
  let fixture: ComponentFixture<SimpleBlankComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimpleBlankComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleBlankComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
