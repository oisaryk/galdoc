import { Component, OnInit } from '@angular/core';
import { HeaderComponent } from '../../layout/header/header.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent extends HeaderComponent implements OnInit {

  constructor() {
    super();
  }

  ngOnInit(): void {
  }

}
