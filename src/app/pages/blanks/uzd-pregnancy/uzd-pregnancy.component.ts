import { Component, OnInit } from '@angular/core';
import { BlankBaseComponent } from '../../../shared/components/blank-base/blank-base.component';
import { FormBuilder } from '@angular/forms';
import { DateService } from '../../../shared/services/date.service';
import { PrintDataService } from '../../../shared/services/print-data.service';
import { Router } from '@angular/router';
import { UzdPregnancyStructure } from '../../../shared/structures/uzd-pregnancy.structure';

@Component({
  selector: 'app-uzd-pregnancy',
  templateUrl: './uzd-pregnancy.component.html',
  styleUrls: ['./uzd-pregnancy.component.scss']
})
export class UzdPregnancyComponent extends BlankBaseComponent implements OnInit {

  defaults = {
    type: 'трансабдомінально',
    polozennya: 'anteflexio',
    shuyka: 'мм',
    vichko: 'замкнуте',
    yaye_exo: 'б/о'
  };

  constructor(
    fb: FormBuilder,
    date: DateService,
    data: PrintDataService,
    private router: Router
  ) {
    super(fb, date, data);
    super.ngOnInit();
  }

  ngOnInit(): void {
    this.options = {
      title: `ПРОТОКОЛ
      ультразвукового дослідження
вагітності`,
      fontSize: 18,
      showDoctors: true,
      borderInValues: true,
      strongTitles: true,
      showEmptyRows: true
    };

    this.form = this.fb.group({
      p_date: [this.currentDate],
      p_type: [this.defaults.type],
      p_first_day_om: [''],
      p_termin_pologiv: [''],
      p_pib: [''],
      p_matka: [''],
      p_polozennya: [this.defaults.polozennya],
      p_dovzyna: [''],
      p_tovshuna: [''],
      p_shuruna: [''],
      p_v: [''],
      p_rozmir: [''],
      p_exostructure: [''],
      p_plivne: [''],
      p_ktr: [''],
      p_sertse: [''],
      p_zhovtochnyy_mishok: [''],
      p_dilyanku: [''],
      p_tonus: [''],
      p_shuyka: [this.defaults.shuyka],
      p_vichko: [this.defaults.vichko],
      p_p_yaye_exostructure: [this.defaults.yaye_exo],
      p_l_yaye_exostructure: [this.defaults.yaye_exo],
      p_riduna_v_duglas: [''],
      p_po_gastatsii: [''],
      p_zakluchennya: [''],
      doctors: ['']
    });

    this.data.blankData.subscribe(options => {
      if (options && options.data && Object.keys(options.data).length !== 0) {
        this.form.patchValue(options.data);
      }
    });
  }

  printCurrentPage() {
    if (this.form.valid) {
      const value = this.form.value;
      // tslint:disable-next-line:max-line-length
      value.p_rozmir = `довжина: ${value.p_dovzyna} мм, товщина: ${value.p_tovshuna} мм, ширина: ${value.p_shuruna} мм. V = ${value.p_v} см3`;

      this.data.updateData({
        ...this.options,
        data: value,
        structure: UzdPregnancyStructure,
        route: 'blank/uzd-pregnancy'
      });

      this.router.navigateByUrl('/print');
    }
  }

}
