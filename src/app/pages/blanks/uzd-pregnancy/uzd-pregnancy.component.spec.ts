import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UzdPregnancyComponent } from './uzd-pregnancy.component';

describe('UzdPregnancyComponent', () => {
  let component: UzdPregnancyComponent;
  let fixture: ComponentFixture<UzdPregnancyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UzdPregnancyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UzdPregnancyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
