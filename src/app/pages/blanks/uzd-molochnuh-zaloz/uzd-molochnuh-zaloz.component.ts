import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { DateService } from '../../../shared/services/date.service';
import { PrintDataService } from '../../../shared/services/print-data.service';
import { Router } from '@angular/router';
import { UzdPregnancyStructure } from '../../../shared/structures/uzd-pregnancy.structure';
import { BlankBaseComponent } from '../../../shared/components/blank-base/blank-base.component';
import { UzdOrganivMalogoTazuStructure } from '../../../shared/structures/uzd-molochnuh-zaloz.structure';

@Component({
  selector: 'app-uzd-molochnuh-zaloz',
  templateUrl: './uzd-molochnuh-zaloz.component.html',
  styleUrls: ['./uzd-molochnuh-zaloz.component.scss']
})
export class UzdMolochnuhZalozComponent extends BlankBaseComponent implements OnInit {

  defaults = {
    type: 'трансабдомінально',
    polozennya: 'anteflexio',
    shuyka: 'мм',
    vichko: 'замкнуте',
    yaye_exo: 'б/о'
  };

  constructor(
    fb: FormBuilder,
    date: DateService,
    data: PrintDataService,
    private router: Router
  ) {
    super(fb, date, data);
    super.ngOnInit();
  }

  ngOnInit(): void {
    this.options = {
      title: `ПП \"Гал - Діагност \"
      ультразвукове дослідження
молочних залоз`,
      fontSize: 18,
      showDoctors: true,
      borderInValues: true,
      strongTitles: true,
      showEmptyRows: true
    };

    this.form = this.fb.group({
      mol_date: [this.currentDate],
      mol_pib: [''],
      mol_menstrual_day: [''],
      mol_right_perevazae: [''],
      mol_pvzk_aneho: [''],
      mol_pvzk_gipo: [''],
      mol_pvzk_getero: [''],
      mol_pvvk_aneho: [''],
      mol_pvvk_gipo: [''],
      mol_pvvk_getero: [''],
      mol_pnzk_aneho: [''],
      mol_pnzk_gipo: [''],
      mol_pnzk_getero: [''],
      mol_pnvk_aneho: [''],
      mol_pnvk_gipo: [''],
      mol_pnvk_getero: [''],
      mol_plds_aneho: [''],
      mol_plds_gipo: [''],
      mol_plds_getero: [''],
      mol_plds: ['б/о'],
      mol_plmh: ['не поширені'],
      mol_prlv: ['не візуалізуються'],
      mol_left_perevazae: [''],
      mol_lvzk_aneho: [''],
      mol_lvzk_gipo: [''],
      mol_lvzk_getero: [''],
      mol_lvvk_aneho: [''],
      mol_lvvk_gipo: [''],
      mol_lvvk_getero: [''],
      mol_lnzk_aneho: [''],
      mol_lnzk_gipo: [''],
      mol_lnzk_getero: [''],
      mol_lnvk_aneho: [''],
      mol_lnvk_gipo: [''],
      mol_lnvk_getero: [''],
      mol_llds: ['б/о'],
      mol_llmh: ['не поширені'],
      mol_lrlv: ['не візуалізуються'],
      mol_zakluchennya: [''],
      mol_recomendovano: [''],
      doctors: ['']
    });

    this.data.blankData.subscribe(options => {
      if (options && options.data && Object.keys(options.data).length !== 0) {
        this.form.patchValue(options.data);
      }
    });
  }

  printCurrentPage() {
    if (this.form.valid) {
      const value = this.form.value;
      this.data.updateData({
        ...this.options,
        data: value,
        structure: UzdOrganivMalogoTazuStructure,
        route: 'blank/uzd-molochnuh-zaloz'
      });

      this.router.navigateByUrl('/print');
    }
  }

}
