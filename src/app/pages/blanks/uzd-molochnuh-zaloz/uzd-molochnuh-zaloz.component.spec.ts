import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UzdMolochnuhZalozComponent } from './uzd-molochnuh-zaloz.component';

describe('UzdMolochnuhZalozComponent', () => {
  let component: UzdMolochnuhZalozComponent;
  let fixture: ComponentFixture<UzdMolochnuhZalozComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UzdMolochnuhZalozComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UzdMolochnuhZalozComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
