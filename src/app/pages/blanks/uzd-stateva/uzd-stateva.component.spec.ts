import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UzdStatevaComponent } from './uzd-stateva.component';

describe('UzdStatevaComponent', () => {
  let component: UzdStatevaComponent;
  let fixture: ComponentFixture<UzdStatevaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UzdStatevaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UzdStatevaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
