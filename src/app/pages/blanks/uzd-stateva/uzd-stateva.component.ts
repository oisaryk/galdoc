import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { DateService } from '../../../shared/services/date.service';
import { PrintDataService } from '../../../shared/services/print-data.service';
import { Router } from '@angular/router';
import { BlankBaseComponent } from '../../../shared/components/blank-base/blank-base.component';
import { UzdStatevaStructure } from '../../../shared/structures/uzd-stateva.structure';

@Component({
  selector: 'app-uzd-stateva',
  templateUrl: './uzd-stateva.component.html',
  styleUrls: ['./uzd-stateva.component.scss']
})
export class UzdStatevaComponent extends BlankBaseComponent implements OnInit {

  constructor(
    fb: FormBuilder,
    date: DateService,
    data: PrintDataService,
    private router: Router
  ) {
    super(fb, date, data);
    super.ngOnInit();
  }

  ngOnInit(): void {
    this.options = {
      title: `ПРОТОКОЛ
      ультразвукового дослідження
      органів сечостатевої системи`,
      fontSize: 16,
      showDoctors: true,
      borderInValues: false,
      strongTitles: true,
      showEmptyRows: true
    };

    this.form = this.fb.group({
      stat_date: [this.currentDate],
      stat_pib: [''],
      stat_p_rozmir: ['   мм.'],
      stat_p_roztash: ['типово'],
      stat_p_perenhimu: ['   мм.'],
      stat_p_eho: ['   мм.'],
      stat_p_lohanka: ['не поширена'],
      stat_p_verh: ['не поширена'],
      stat_p_kalts: ['немає'],
      stat_p_concrements: ['немає'],
      stat_p_dribni_exo: ['без УЗ-доріжки'],
      stat_l_rozmir: ['   мм.'],
      stat_l_roztash: ['типово'],
      stat_l_perenhimu: ['   мм.'],
      stat_l_eho: ['   мм.'],
      stat_l_lohanka: ['не поширена'],
      stat_l_verh: ['не поширена'],
      stat_l_kalts: ['немає'],
      stat_l_concrements: ['немає'],
      stat_l_dribni_exo: ['без УЗ-доріжки'],
      stat_mihur: [''],
      stat_results: [''],
      doctors: ['']
    });

    this.data.blankData.subscribe(options => {
      if (options && options.data && Object.keys(options.data).length !== 0) {
        this.form.patchValue(options.data);
      }
    });
  }

  printCurrentPage() {
    if (this.form.valid) {
      const value = this.form.value;
      this.data.updateData({
        ...this.options,
        data: value,
        structure: UzdStatevaStructure,
        route: 'blank/uzd-stateva'
      });

      this.router.navigateByUrl('/print');
    }
  }
}
