import { Component, OnInit } from '@angular/core';
import { BlankBaseComponent } from '../../../shared/components/blank-base/blank-base.component';
import { FormBuilder } from '@angular/forms';
import { DateService } from '../../../shared/services/date.service';
import { PrintDataService } from '../../../shared/services/print-data.service';
import { Router } from '@angular/router';
import { UzdProtocolOneUzStructure } from '../../../shared/structures/uzd-protocol-one-uz.structure';

@Component({
  selector: 'app-uzd-protocol-one-uz',
  templateUrl: './uzd-protocol-one-uz.component.html',
  styleUrls: ['./uzd-protocol-one-uz.component.scss']
})
export class UzdProtocolOneUzComponent extends BlankBaseComponent implements OnInit {

  constructor(
    fb: FormBuilder,
    date: DateService,
    data: PrintDataService,
    private router: Router
  ) {
    super(fb, date, data);
    super.ngOnInit();
  }

  ngOnInit(): void {
    this.options = {
      title: `ПРОТОКОЛ
      ультразвукового дослідження вагітних
      І УЗ-обстеження (11 тиж. + 1 день - 13 тиж. + 6 днів)`,
      fontSize: 18,
      showDoctors: true,
      borderInValues: false,
      strongTitles: true,
      showEmptyRows: true
    };

    this.form = this.fb.group({
      one_date: [this.currentDate],
      one_pib: [''],
      one_om: [''],
      one_gastatsia: [''],
      one_termin: [''],
      one_method: ['трансабдомінальна'],
      one_dovz: [''],
      one_shuruna: [''],
      one_pered_zad: [''],
      one_structura_biomentiyu: ['не змінена'],
      one_tonus: ['не підвищений'],
      one_local_hipertonus: [''],
      one_u_poroz_vizial: ['одне плідне яйце правильної форми'],
      one_diamert_egg: [' мм.'],
      one_kilkist_embrioniv: ['один'],
      one_vidsharuvannya: [''],
      one_kupriko: [' мм'],
      one_biparietal: [' мм'],
      one_skull_bones: ['візуалізуються'],
      one_okruznist: [' мм'],
      one_ser_diametr: [' мм'],
      one_dov_stegna: [' мм'],
      one_komirts_prostir: [' мм'],
      one_nose_kist: [' мм ( візуалізується )'],
      one_hearth: [''],
      one_hearth_bom: ['є'],
      one_chss: [' уд./хв'],
      one_pered_cherevna: ['цілісна'],
      one_ruh_ploda: ['нормальна'],
      one_local_horionu: ['передня'],
      one_tovshuna: [' мм'],
      one_structura: ['б/о'],
      one_shuyka: [' мм'],
      one_tserv_canal: [' мм'],
      one_diam_vnutrishnogo: [' мм'],
      one_stan_prudatkiv: ['б/о'],
      one_osobluvosti: [''],
      one_results: [''],
      doctors: ['']
    });

    this.data.blankData.subscribe(options => {
      if (options && options.data && Object.keys(options.data).length !== 0) {
        this.form.patchValue(options.data);
      }
    });
  }

  printCurrentPage() {
    if (this.form.valid) {
      const value = this.form.value;
      this.data.updateData({
        ...this.options,
        data: value,
        structure: UzdProtocolOneUzStructure,
        route: 'blank/uzd-protocol-one-uz'
      });

      this.router.navigateByUrl('/print');
    }
  }

}
