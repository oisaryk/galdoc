import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UzdProtocolOneUzComponent } from './uzd-protocol-one-uz.component';

describe('UzdProtocolOneUzComponent', () => {
  let component: UzdProtocolOneUzComponent;
  let fixture: ComponentFixture<UzdProtocolOneUzComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UzdProtocolOneUzComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UzdProtocolOneUzComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
