import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { DateService } from '../../../shared/services/date.service';
import { PrintDataService } from '../../../shared/services/print-data.service';
import { Router } from '@angular/router';
import { UzdDoplerometryStructure } from '../../../shared/structures/uzd-doplerometry.structure';
import { BlankBaseComponent } from '../../../shared/components/blank-base/blank-base.component';
import { UzdTwoThreeUzStructure } from '../../../shared/structures/uzd-two-three-uz.structure';

@Component({
  selector: 'app-uzd-two-three-uz',
  templateUrl: './uzd-two-three-uz.component.html',
  styleUrls: ['./uzd-two-three-uz.component.scss']
})
export class UzdTwoThreeUzComponent extends BlankBaseComponent implements OnInit {

  constructor(
    fb: FormBuilder,
    date: DateService,
    data: PrintDataService,
    private router: Router
  ) {
    super(fb, date, data);
    super.ngOnInit();
  }

  ngOnInit(): void {
    this.options = {
      title: `ПРИВАТНЕ ПІДПРИЄМСТВО \"ГАЛ-ДІАГНОСТ \"
II-III УЗ - обстеження`,
      fontSize: 18,
      showDoctors: true,
      borderInValues: true,
      strongTitles: true,
      showEmptyRows: true
    };

    this.form = this.fb.group({
      uz_date: [this.currentDate],
      uz_pib: [''],
      uz_plodiv_count: ['один'],
      uz_vaga: [''],
      uz_poloz_ploda: ['поздовжнє'],
      uz_predlezannya: ['головне'],
      uz_sertce: ['ритмічне'],
      uz_chss: ['уд/хв.'],
      uz_ruh_ploda: ['+'],
      uz_bpr: ['  мм.'],
      uz_brain_str: ['б/о'],
      uz_og: [' мм.'],
      uz_noso_triangle: ['б/о'],
      uz_lpr: ['  мм.'],
      uz_bsh: ['  мм.'],
      uz_pot_rog: ['  мм.'],
      uz_chkam: ['  мм.'],
      uz_ao: ['  мм.'],
      uz_leg_art: ['  мм.'],
      uz_zriz_tr: ['N'],
      uz_leg_st: ['зрілі'],
      uz_mozochok: ['  мм.'],
      uz_kistochka: ['  мм.'],
      uz_dc: [''],
      uz_ozh: ['  мм.'],
      uz_sdgk: ['  мм.'],
      uz_kilkist_vod: ['нормальна'],
      uz_index: [''],
      uz_platsenty: ['передня'],
      uz_stupin_zrilosti: ['0'],
      uz_nizniy_kray: ['високо'],
      uz_nizniy_kray_input: ['високо'],
      uz_tovshuna: ['  мм.'],
      uz_patolog_platsenty: [''],
      uz_zrizu_petel: ['ні'],
      uz_zrizu_petel_input: [''],
      uz_pupovyna: ['дві'],
      uz_pupo_kilce: ['  мм.'],
      uz_shuyka: ['  мм.'],
      uz_rubets: ['товщина міометрію в ділянці рубця   мм., в нижньому сегменті -   мм, по передній стінці   мм.'],
      uz_vnvichko: [''],
      uz_vnvichko_input: ['  мм.'],
      uz_matka_input: [''],
      uz_tservik: [''],
      uz_terminpogest: ['  т  д'],
      uz_termin: [''],
      uz_sd: ['норма'],
      uz_krovoplun: ['норма'],
      uz_cma: [''],
      uz_ir: [''],
      uz_pi: [''],
      uz_result: [''],
      doctors: ['']
    });

    this.data.blankData.subscribe(options => {
      if (options && options.data && Object.keys(options.data).length !== 0) {
        this.form.patchValue(options.data);
      }
    });
  }

  printCurrentPage() {
    if (this.form.valid) {
      const value = this.form.value;
      this.data.updateData({
        ...this.options,
        data: value,
        structure: UzdTwoThreeUzStructure,
        route: 'blank/uzd-two-three-uz'
      });

      this.router.navigateByUrl('/print');
    }
  }
}
