import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UzdTwoThreeUzComponent } from './uzd-two-three-uz.component';

describe('UzdTwoThreeUzComponent', () => {
  let component: UzdTwoThreeUzComponent;
  let fixture: ComponentFixture<UzdTwoThreeUzComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UzdTwoThreeUzComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UzdTwoThreeUzComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
