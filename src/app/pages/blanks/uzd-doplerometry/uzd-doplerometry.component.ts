import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { DateService } from '../../../shared/services/date.service';
import { PrintDataService } from '../../../shared/services/print-data.service';
import { Router } from '@angular/router';
import { BlankBaseComponent } from '../../../shared/components/blank-base/blank-base.component';
import { UzdDoplerometryStructure } from '../../../shared/structures/uzd-doplerometry.structure';

@Component({
  selector: 'app-uzd-doplerometry',
  templateUrl: './uzd-doplerometry.component.html',
  styleUrls: ['./uzd-doplerometry.component.scss']
})
export class UzdDoplerometryComponent extends BlankBaseComponent implements OnInit {

  constructor(
    fb: FormBuilder,
    date: DateService,
    data: PrintDataService,
    private router: Router
  ) {
    super(fb, date, data);
    super.ngOnInit();
  }

  ngOnInit(): void {
    this.options = {
      title: `ПП \"Гал - Діагност \"
ДОПЛЕРОМЕТРІЯ`,
      fontSize: 18,
      showDoctors: true,
      borderInValues: true,
      strongTitles: true,
      showEmptyRows: true
    };

    this.form = this.fb.group({
      dp_date: [this.currentDate],
      dp_pib: [],
      dp_polozennya: [''],
      dp_rubets: ['товщина міометрію в ділянці рубця   мм., в нижньому сегменті -   мм, по передній стінці   мм.'],
      dp_peredlezannya: ['головне'],
      dp_sertse: ['ритмічне'],
      dp_tss: [' уд/хв.'],
      dp_moves: ['активні'],

      dp_art_sd: [''],
      dp_art_ir: [''],
      dp_art_pi: [''],
      dp_art_krovo: ['нормальний'],

      dp_mat_sd: [''],
      dp_mat_ir: [''],
      dp_mat_pi: [''],
      dp_mat_krovo: ['нормальний'],

      dp_liva_ir: [''],
      dp_liva_pi: [''],
      dp_liva_krovo: ['нормальний'],

      dp_prava_ir: [''],
      dp_prava_pi: [''],
      dp_prava_krovo: ['нормальний'],
      doctors: ['']
    });

    this.data.blankData.subscribe(options => {
      if (options && options.data && Object.keys(options.data).length !== 0) {
        this.form.patchValue(options.data);
      }
    });
  }

  printCurrentPage() {
    if (this.form.valid) {
      const value = this.form.value;
      this.data.updateData({
        ...this.options,
        data: value,
        structure: UzdDoplerometryStructure,
        route: 'blank/uzd-doplerometry'
      });

      this.router.navigateByUrl('/print');
    }
  }

}
