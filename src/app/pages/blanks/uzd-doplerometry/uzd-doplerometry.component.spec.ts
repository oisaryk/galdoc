import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UzdDoplerometryComponent } from './uzd-doplerometry.component';

describe('UzdDoplerometryComponent', () => {
  let component: UzdDoplerometryComponent;
  let fixture: ComponentFixture<UzdDoplerometryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UzdDoplerometryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UzdDoplerometryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
