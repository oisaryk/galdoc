import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UzdTservikometriaComponent } from './uzd-tservikometria.component';

describe('UzdTservikometriaComponent', () => {
  let component: UzdTservikometriaComponent;
  let fixture: ComponentFixture<UzdTservikometriaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UzdTservikometriaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UzdTservikometriaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
