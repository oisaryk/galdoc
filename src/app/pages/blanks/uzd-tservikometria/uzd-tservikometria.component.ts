import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { DateService } from '../../../shared/services/date.service';
import { PrintDataService } from '../../../shared/services/print-data.service';
import { Router } from '@angular/router';
import { BlankBaseComponent } from '../../../shared/components/blank-base/blank-base.component';
import { UzdTservikometriaStructure } from '../../../shared/structures/uzd-tservikometria.structure';

@Component({
  selector: 'app-uzd-tservikometria',
  templateUrl: './uzd-tservikometria.component.html',
  styleUrls: ['./uzd-tservikometria.component.scss']
})
export class UzdTservikometriaComponent extends BlankBaseComponent implements OnInit {

  constructor(
    fb: FormBuilder,
    date: DateService,
    data: PrintDataService,
    private router: Router
  ) {
    super(fb, date, data);
    super.ngOnInit();
  }

  ngOnInit(): void {
    this.options = {
      title: `ПП \"Гал - Діагност \"
      ЦЕРВІКОМЕТРІЯ`,
      fontSize: 18,
      showDoctors: true,
      borderInValues: true,
      strongTitles: true,
      showEmptyRows: true
    };

    this.form = this.fb.group({
      ts_date: [this.currentDate],
      ts_pib: [],
      ts_shuyka: [],
      ts_canal: [],
      ts_vichko: ['замкнуте'],
      ts_sertse: ['ритмічне'],
      ts_chss: ['уд/хв'],
      ts_moves: ['+'],
      ts_vusnovok: [''],
      doctors: ['']
    });

    this.data.blankData.subscribe(options => {
      if (options && options.data && Object.keys(options.data).length !== 0) {
        this.form.patchValue(options.data);
      }
    });
  }

  printCurrentPage() {
    if (this.form.valid) {
      const value = this.form.value;
      this.data.updateData({
        ...this.options,
        data: value,
        structure: UzdTservikometriaStructure,
        route: 'blank/uzd-tservikometria'
      });

      this.router.navigateByUrl('/print');
    }
  }
}
