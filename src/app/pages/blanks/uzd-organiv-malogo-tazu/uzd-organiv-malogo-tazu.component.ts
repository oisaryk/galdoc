import { Component, OnInit } from '@angular/core';
import { BlankBaseComponent } from '../../../shared/components/blank-base/blank-base.component';
import { FormBuilder } from '@angular/forms';
import { DateService } from '../../../shared/services/date.service';
import { PrintDataService } from '../../../shared/services/print-data.service';
import { Router } from '@angular/router';
import { UzdOrganivMalogoTazuStructure } from '../../../shared/structures/uzd-organiv-malogo-tazu.structure';

@Component({
  selector: 'app-uzd-organiv-malogo-tazu',
  templateUrl: './uzd-organiv-malogo-tazu.component.html',
  styleUrls: ['./uzd-organiv-malogo-tazu.component.scss']
})
export class UzdOrganivMalogoTazuComponent extends BlankBaseComponent implements OnInit {

  defaults = {
    type: 'трансабдомінально',
    polozennya: 'anteflexio',
    endometrii: ' мм.',
    shuyka: '  x  мм.',
    yayechnyk: 'ехоструктура відповідає віку та фазі циклу',
    rozmir: '  x  x  мм; V=  см3'
  };

  constructor(
    fb: FormBuilder,
    date: DateService,
    data: PrintDataService,
    private router: Router
  ) {
    super(fb, date, data);
    super.ngOnInit();
  }

  ngOnInit(): void {
    this.options = {
      title: `ПРОТОКОЛ
      ультразвукового дослідження
      органів малого тазу жінки`,
      fontSize: 18,
      showDoctors: true,
      borderInValues: true,
      strongTitles: true,
      showEmptyRows: true
    };

    this.form = this.fb.group({
      omt_date: [this.currentDate],
      omt_type: [this.defaults.type],
      omt_menstrual: [''],
      omt_pib: [''],
      omt_matka: [''],
      omt_polozennya: [this.defaults.polozennya],
      omt_dovzyna: [''],
      omt_tovshuna: [''],
      omt_shuruna: [''],
      omt_v: [''],
      omt_rozmir: [''],
      omt_exostructure: [''],
      omt_perednya_stinka: [''],
      omt_zadnya_stinka: [''],
      omt_dno: [''],
      omt_endometrii: [this.defaults.endometrii],
      omt_poroznyna: [this.defaults.endometrii],
      omt_shuyka: [this.defaults.shuyka],
      omt_tservikalnyy_kanal: [''],
      omt_right_yayechnyk: [this.defaults.yayechnyk],
      omt_right_rozstashuvannya: [''],
      omt_rigth_rozmir: [this.defaults.rozmir],
      omt_rigth_v_structuri: [''],
      omt_right_truba: [''],
      omt_left_yayechnyk: [this.defaults.yayechnyk],
      omt_left_rozstashuvannya: [''],
      omt_left_rozmir: [this.defaults.rozmir],
      omt_left_v_structuri: [''],
      omt_left_truba: [''],
      omt_riduna_v_duglas: [''],
      omt_zakluchennya: [''],
      omt_rekomendatsii: [''],
      doctors: ['']
    });

    this.data.blankData.subscribe(options => {
      if (options && options.data && Object.keys(options.data).length !== 0) {
        this.form.patchValue(options.data);
      }
    });
  }

  printCurrentPage() {
    if (this.form.valid) {
      const value = this.form.value;
      // tslint:disable-next-line:max-line-length
      value.omt_rozmir = `довжина: ${value.omt_dovzyna} мм, товщина: ${value.omt_tovshuna} мм, ширина: ${value.omt_shuruna} мм. V = ${value.omt_v} см3`;

      this.data.updateData({
        ...this.options,
        data: value,
        structure: UzdOrganivMalogoTazuStructure,
        route: 'blank/uzd-organiv-malogo-tazu'
      });

      this.router.navigateByUrl('/print');
    }
  }

}
