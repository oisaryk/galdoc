import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UzdOrganivMalogoTazuComponent } from './uzd-organiv-malogo-tazu.component';

describe('UzdOrganivMalogoTazuComponent', () => {
  let component: UzdOrganivMalogoTazuComponent;
  let fixture: ComponentFixture<UzdOrganivMalogoTazuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UzdOrganivMalogoTazuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UzdOrganivMalogoTazuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
