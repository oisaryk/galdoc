import { Component, OnInit } from '@angular/core';
import { BlankBaseComponent } from '../../../shared/components/blank-base/blank-base.component';
import { FormBuilder } from '@angular/forms';
import { DateService } from '../../../shared/services/date.service';
import { PrintDataService } from '../../../shared/services/print-data.service';
import { Router } from '@angular/router';
import { UzdProtocolTwoUzStructure } from '../../../shared/structures/uzd-protocol-two-uz.structure';

@Component({
  selector: 'app-uzd-protocol-two-uz',
  templateUrl: './uzd-protocol-two-uz.component.html',
  styleUrls: ['./uzd-protocol-two-uz.component.scss']
})
export class UzdProtocolTwoUzComponent extends BlankBaseComponent implements OnInit {

  constructor(
    fb: FormBuilder,
    date: DateService,
    data: PrintDataService,
    private router: Router
  ) {
    super(fb, date, data);
    super.ngOnInit();
  }

  ngOnInit(): void {
    this.options = {
      title: `ПРОТОКОЛ
      ультразвукового дослідження вагітних
      ІІ УЗ-обстеження (18-21 тиждень)`,
      fontSize: 16,
      showDoctors: true,
      borderInValues: false,
      strongTitles: true,
      showEmptyRows: true
    };

    this.form = this.fb.group({
      two_date: [this.currentDate],
      two_pib: [''],
      two_om: [''],
      two_gastatsia: ['т. д.'],
      two_termin: [''],
      two_vaga: [''],
      two_plodiv: ['один'],
      two_poloz: ['поздовжнє'],
      two_pred: ['головне'],
      two_sertse: ['ритмічне'],
      two_chss: ['уд/хв'],
      two_moves: ['+'],
      two_sered_exo: [' мм'],
      two_zadni_roga: [' мм'],
      two_bok_sh_mozku: [' мм'],
      two_big_tsusterna: [' мм'],
      two_diafragma: ['б/о'],
      two_perednya_cherevna: ['цілісна, пупочне -  мм.'],
      two_kushechnuk: ['ехогенність: не підвищена, петлі не розширені'],
      two_stovbur: ['б/о'],
      // tslint:disable-next-line:max-line-length
      two_hearth: ['положення нормальне, 4-камерний зріз серця: візуалізується:  мм, хід магістральних судин серця правильний: легенева артерія -  мм, аорта -  мм.'],
      two_bpr: [''],
      two_lpr: [''],
      two_tsefal_index: [''],
      two_head_okr: [''],
      two_setch_mikhur: [''],
      two_mozochok: [''],
      two_intraokul_rozmir: [''],
      two_sdgk: [''],
      two_diam_zuv: [''],
      two_okr_zuvota: [''],
      two_shlunok: [''],
      two_nurka_right: [''],
      two_nurka_left: [''],
      two_dovz_stegna: [''],
      two_vel_bedr_kist: [''],
      two_dozv_mal_bedr_kist: [''],
      two_stopa: [''],
      two_dovz_plecha: [''],
      two_dovz_likt: [''],
      two_sertse_mm: [''],
      two_dovz_promenevoi: [''],
      two_kust: [''],
      two_luts_rozshilunu: ['не візуалізуються'],
      two_nosova_kistka: [' мм'],
      two_rozmir_skladku: [' мм'],
      two_kilkist_vod: [''],
      two_local_plats: [''],
      two_nuz_platsenty: [' мм від вн. вічка'],
      two_tov_platsenty: [' мм'],
      two_str_platsenty: [''],
      two_revers_krovotok: ['не визначається'],
      two_kilk_sudun: ['3'],
      two_obv_pupovunu: ['ні'],
      two_pered_sudun: ['ні'],
      two_obol_prukrip: ['ні'],
      two_shuyka_matku: [' мм'],
      two_vnut_vichko: ['замкнуте'],
      two_pidozra: [''],
      two_results: [''],
      two_recomends: [''],
      doctors: ['']
    });

    this.data.blankData.subscribe(options => {
      if (options && options.data && Object.keys(options.data).length !== 0) {
        this.form.patchValue(options.data);
      }
    });
  }

  printCurrentPage() {
    if (this.form.valid) {
      const value = this.form.value;
      this.data.updateData({
        ...this.options,
        data: value,
        structure: UzdProtocolTwoUzStructure,
        route: 'blank/uzd-protocol-two-uz'
      });

      this.router.navigateByUrl('/print');
    }
  }

}
