import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UzdProtocolTwoUzComponent } from './uzd-protocol-two-uz.component';

describe('UzdProtocolTwoUzComponent', () => {
  let component: UzdProtocolTwoUzComponent;
  let fixture: ComponentFixture<UzdProtocolTwoUzComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UzdProtocolTwoUzComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UzdProtocolTwoUzComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
