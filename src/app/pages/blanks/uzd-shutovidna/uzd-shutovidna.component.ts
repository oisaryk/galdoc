import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { DateService } from '../../../shared/services/date.service';
import { PrintDataService } from '../../../shared/services/print-data.service';
import { Router } from '@angular/router';
import { BlankBaseComponent } from '../../../shared/components/blank-base/blank-base.component';
import { UzdShitovidnaStructure } from '../../../shared/structures/uzd-shitovidna.structure';

@Component({
  selector: 'app-uzd-shutovidna',
  templateUrl: './uzd-shutovidna.component.html',
  styleUrls: ['./uzd-shutovidna.component.scss']
})
export class UzdShutovidnaComponent extends BlankBaseComponent implements OnInit {

  constructor(
    fb: FormBuilder,
    date: DateService,
    data: PrintDataService,
    private router: Router
  ) {
    super(fb, date, data);
    super.ngOnInit();
  }

  ngOnInit(): void {
    this.options = {
      title: `ПРОТОКОЛ
      ультразвукового дослідження щитовидної залози №___`,
      fontSize: 16,
      showDoctors: true,
      borderInValues: false,
      strongTitles: true,
      showEmptyRows: true
    };

    this.form = this.fb.group({
      sh_date: [this.currentDate],
      sh_pib: [''],
      sh_vik: [''],
      sh_roztash: ['типово'],
      sh_capsula: [''],
      sh_p_chastka: ['   см3'],
      sh_p_eho: [''],
      sh_p_strycture: [''],
      sh_l_chastka: ['   см3'],
      sh_l_eho: [''],
      sh_l_strycture: [''],
      sh_pereshuyok: [''],
      sh_zagal_ob: ['   см3'],
      sh_results: [''],
      sh_recomends: [''],
      doctors: ['']
    });

    this.data.blankData.subscribe(options => {
      if (options && options.data && Object.keys(options.data).length !== 0) {
        this.form.patchValue(options.data);
      }
    });
  }

  printCurrentPage() {
    if (this.form.valid) {
      const value = this.form.value;
      this.data.updateData({
        ...this.options,
        data: value,
        structure: UzdShitovidnaStructure,
        route: 'blank/uzd-shitovidna'
      });

      this.router.navigateByUrl('/print');
    }
  }
}
