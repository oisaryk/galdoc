import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UzdShutovidnaComponent } from './uzd-shutovidna.component';

describe('UzdShutovidnaComponent', () => {
  let component: UzdShutovidnaComponent;
  let fixture: ComponentFixture<UzdShutovidnaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UzdShutovidnaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UzdShutovidnaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
