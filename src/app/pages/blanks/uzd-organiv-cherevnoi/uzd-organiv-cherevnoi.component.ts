import { Component, OnInit } from '@angular/core';
import { BlankBaseComponent } from '../../../shared/components/blank-base/blank-base.component';
import { FormBuilder } from '@angular/forms';
import { DateService } from '../../../shared/services/date.service';
import { PrintDataService } from '../../../shared/services/print-data.service';
import { Router } from '@angular/router';
import { UzdCherevnoiStructure } from '../../../shared/structures/uzd-cherevnoi.structure';

@Component({
  selector: 'app-uzd-organiv-cherevnoi',
  templateUrl: './uzd-organiv-cherevnoi.component.html',
  styleUrls: ['./uzd-organiv-cherevnoi.component.scss']
})
export class UzdOrganivCherevnoiComponent extends BlankBaseComponent implements OnInit {

  constructor(
    fb: FormBuilder,
    date: DateService,
    data: PrintDataService,
    private router: Router
  ) {
    super(fb, date, data);
    super.ngOnInit();
  }

  ngOnInit(): void {
    this.options = {
      title: `ПРОТОКОЛ
      ультразвукового дослідження черевної порожнини`,
      fontSize: 16,
      showDoctors: true,
      borderInValues: false,
      strongTitles: true,
      showEmptyRows: true
    };

    this.form = this.fb.group({
      ch_date: [this.currentDate],
      ch_pib: [''],
      ch_konturs: ['рівні'],
      ch_zber: [''],
      ch_tov_prav_doli: ['   мм'],
      ch_tov_liv_doli: ['   мм'],
      ch_parenhimu: ['однорідна'],
      ch_exo: [''],
      ch_v_portae: [''],
      ch_sud_mal: ['збережений'],
      ch_holedoh: [''],
      ch_zov_hodu: [''],
      ch_z_rozmir: ['   мм'],
      ch_z_perevib: ['ні'],
      ch_z_pered_stinka: [''],
      ch_z_sluz_po_zadniy: ['ні'],
      ch_z_obyem: ['  см3'],
      ch_zaloza: [''],
      ch_zaloza_golivka: ['   мм'],
      ch_zaloza_tilo: ['   мм'],
      ch_zaloza_hvist: ['   мм'],
      ch_zaloza_selezinka: [''],
      ch_p_nurka: ['  мм'],
      ch_p_nurka_parenhima: ['  мм'],
      ch_p_nurka_chpk: ['  мм'],
      ch_p_nurka_segment: [''],
      ch_p_nurka_ps: ['непоширена'],
      ch_l_nurka: ['  мм'],
      ch_l_nurka_parenhima: ['  мм'],
      ch_l_nurka_chpk: ['  мм'],
      ch_l_nurka_segment: [''],
      ch_l_nurka_ps: ['непоширена'],
      ch_mihur: [''],
      ch_results: [''],
      doctors: ['']
    });

    this.data.blankData.subscribe(options => {
      if (options && options.data && Object.keys(options.data).length !== 0) {
        this.form.patchValue(options.data);
      }
    });
  }

  printCurrentPage() {
    if (this.form.valid) {
      const value = this.form.value;
      this.data.updateData({
        ...this.options,
        data: value,
        structure: UzdCherevnoiStructure,
        route: 'blank/uzd-organiv-cherevnoi'
      });

      this.router.navigateByUrl('/print');
    }
  }
}
