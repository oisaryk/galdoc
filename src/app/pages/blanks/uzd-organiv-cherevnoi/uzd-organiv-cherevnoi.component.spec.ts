import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UzdOrganivCherevnoiComponent } from './uzd-organiv-cherevnoi.component';

describe('UzdOrganivCherevnoiComponent', () => {
  let component: UzdOrganivCherevnoiComponent;
  let fixture: ComponentFixture<UzdOrganivCherevnoiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UzdOrganivCherevnoiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UzdOrganivCherevnoiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
