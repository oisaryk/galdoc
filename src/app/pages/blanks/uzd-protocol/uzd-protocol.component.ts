import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { DateService } from '../../../shared/services/date.service';
import { PrintDataService } from '../../../shared/services/print-data.service';
import { Router } from '@angular/router';
import { BlankBaseComponent } from '../../../shared/components/blank-base/blank-base.component';
import { UzdProtocolStructure } from '../../../shared/structures/uzd-protocol.structure';

@Component({
  selector: 'app-uzd-protocol',
  templateUrl: './uzd-protocol.component.html',
  styleUrls: ['./uzd-protocol.component.scss']
})
export class UzdProtocolComponent extends BlankBaseComponent implements OnInit {

  constructor(
    fb: FormBuilder,
    date: DateService,
    data: PrintDataService,
    private router: Router
  ) {
    super(fb, date, data);
    super.ngOnInit();
  }

  ngOnInit(): void {
    this.options = {
      title: `ПП \"Гал - Діагност \"
      Протокол ультразвукового дослідження`,
      fontSize: 18,
      showDoctors: true,
      borderInValues: true,
      strongTitles: true,
      showEmptyRows: true
    };

    this.form = this.fb.group({
      pro_date: [this.currentDate],
      pro_pib: [''],
      pro_sertse: ['ритмічне'],
      pro_chss: ['+'],
      pro_moves: ['+'],
      pro_dilyanku: [''],
      pro_tonus: [''],
      doctors: ['']
    });

    this.data.blankData.subscribe(options => {
      if (options && options.data && Object.keys(options.data).length !== 0) {
        this.form.patchValue(options.data);
      }
    });
  }

  printCurrentPage() {
    if (this.form.valid) {
      const value = this.form.value;
      this.data.updateData({
        ...this.options,
        data: value,
        structure: UzdProtocolStructure,
        route: 'blank/uzd-protocol'
      });

      this.router.navigateByUrl('/print');
    }
  }

}
