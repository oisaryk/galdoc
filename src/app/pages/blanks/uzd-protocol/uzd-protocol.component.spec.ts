import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UzdProtocolComponent } from './uzd-protocol.component';

describe('UzdProtocolComponent', () => {
  let component: UzdProtocolComponent;
  let fixture: ComponentFixture<UzdProtocolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UzdProtocolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UzdProtocolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
