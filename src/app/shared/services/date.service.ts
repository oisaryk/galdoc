import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';

@Injectable({
  providedIn: 'root'
})

export class DateService {

  uaDateMatch = {
    '01' : 'січня',
    '02' : 'лютого',
    '03' : 'березня',
    '04' : 'квітня',
    '05' : 'травня',
    '06' : 'червня',
    '07' : 'липня',
    '08' : 'серпня',
    '09' : 'вересня',
    10 : 'жовтня',
    11 : 'листопада',
    12 : 'грудня'
  };

  currentDate = new Date();
  day: string;
  month: string;
  year: string;

  constructor(private date: DatePipe) {
    this.day = this.date.transform(this.currentDate, 'dd');
    this.month = this.date.transform(this.currentDate, 'MM');
    this.year = this.date.transform(this.currentDate, 'yyyy');
  }

  getFullDate() {
    return this.currentDate;
  }

  getCurrentDay() {
    return this.day;
  }

  getCurrentMonth() {
    return this.month;
  }

  getCurrentYear() {
    return this.year;
  }

  getCurrentMonthLocale() {
    return this.uaDateMatch[this.month];
  }

  getCurrentUaDate() {
    return `« ${this.day} » ${this.month} ${this.year}р.`;
  }

}
