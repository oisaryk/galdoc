import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PrintDataService {

  initState = {
    data: {},
    options: {},
    structure: [{}]
  };

  private blankDataBehaviour = new BehaviorSubject(this.initState);
  blankData = this.blankDataBehaviour.asObservable();

  updateData(data) {
    this.blankDataBehaviour.next(data);
  }

}
