export interface BlankStructureInterface {
  type: 'single' | 'double' | 'triple' | 'full-table' | 'title';
  align?: string;
  strong?: boolean;
  items?: {
    title?: string,
    field?: string,
    class?: string
  }[];
  table_headers?: {
    title: string;
    class: string;
  }[];
  table_rows?: {
    title?: string;
    field?: string;
    class?: string;
  }[][];
}
