export interface PrintDataInterface {
  showDoctors?: boolean;
  data?: {};
  logoSize?: number;
  deviceSize?: number;
  options?: {};
  fontSize?: number;
  lineHeight?: number;
  titleSize?: number;
  title?: string;
  structure?: {}[];
  route?: string;
  borderInValues?: boolean;
  strongTitles?: boolean;
  showEmptyRows?: boolean;
}
