import { BlankStructureInterface } from '../interfaces/structure';

export const UzdProtocolStructure: BlankStructureInterface[] = [
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: '',
        field: 'pro_date'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'П.І.Б:',
        field: 'pro_pib'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Серцебиття:',
        field: 'pro_sertse'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'ЧСС:',
        field: 'pro_chss'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Рухи:',
        field: 'pro_moves'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Ділянки відшарування плідного яйця:',
        field: 'pro_dilyanku'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Тонус мінометрію:',
        field: 'pro_tonus'
      }
    ]
  },
  {
    type: 'single',
    align: 'right',
    items: [
      {
        title: 'Лікар:',
        field: 'doctors'
      }
    ]
  }
];
