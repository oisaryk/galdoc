import { BlankStructureInterface } from '../interfaces/structure';

export const UzdStatevaStructure: BlankStructureInterface[] = [
  {
    type: 'single',
    align: 'center',
    items: [
      {
        title: '',
        field: 'stat_date'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'П.І.Б:',
        field: 'stat_pib'
      }
    ]
  },
  {
    type: 'single',
    align: 'left top-10',
    items: [
      {
        title: 'Права нирка розмірами:',
        field: 'stat_p_rozmir'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Розташована:',
        field: 'stat_p_roztash'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Товщина паренхіми:',
        field: 'stat_p_perenhimu'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Ч.Л.К. підвищеної ехогенності:',
        field: 'stat_p_eho'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Лоханка:',
        field: 'stat_p_lohanka'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Верхня третина сечоводу:',
        field: 'stat_p_verh'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Наявність кальцифінованих судин:',
        field: 'stat_p_kalts'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Конкременти:',
        field: 'stat_p_concrements'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Дрібні ехо (+) включення:',
        field: 'stat_p_dribni_exo'
      }
    ]
  },
  {
    type: 'single',
    align: 'left top-10',
    items: [
      {
        title: 'Ліва нирка розмірами:',
        field: 'stat_l_rozmir'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Розташована:',
        field: 'stat_l_roztash'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Товщина паренхіми:',
        field: 'stat_l_perenhimu'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Ч.Л.К. підвищеної ехогенності:',
        field: 'stat_l_eho'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Лоханка:',
        field: 'stat_l_lohanka'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Верхня третина сечоводу:',
        field: 'stat_l_verh'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Наявність кальцифінованих судин:',
        field: 'stat_l_kalts'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Конкременти:',
        field: 'stat_l_concrements'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Дрібні ехо (+) включення:',
        field: 'stat_l_dribni_exo'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Сечовий міхур:',
        field: 'stat_mihur'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'ЗАКЛЮЧЕННЯ:',
        field: 'stat_results'
      }
    ]
  },
  {
    type: 'single',
    align: 'right margin-10',
    items: [
      {
        title: 'Лікар:',
        field: 'doctors'
      }
    ]
  }
];
