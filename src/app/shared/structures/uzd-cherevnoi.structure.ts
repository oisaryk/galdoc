import { BlankStructureInterface } from '../interfaces/structure';

export const UzdCherevnoiStructure: BlankStructureInterface[] = [
  {
    type: 'single',
    align: 'center',
    items: [
      {
        title: '',
        field: 'ch_date'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'П.І.Б:',
        field: 'ch_pib'
      }
    ]
  },
  {
    type: 'double',
    align: 'left',
    items: [
      {
        title: 'Контури печінки:',
        field: 'ch_konturs'
      },
      {
        title: 'збережені:',
        field: 'ch_zber'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Товщина правої долі:',
        field: 'ch_tov_prav_doli'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Товщина лівої долі:',
        field: 'ch_tov_liv_doli'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Структура паренхіми:',
        field: 'ch_parenhimu'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Ехогенність:',
        field: 'ch_exo'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'V.Portae:',
        field: 'ch_v_portae'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Судинний малюнок:',
        field: 'ch_sud_mal'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Холєдох:',
        field: 'ch_holedoh'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Жовчні ходи:',
        field: 'ch_zov_hodu'
      }
    ]
  },
  {
    type: 'title',
    align: 'left',
    items: [
      {
        title: 'Жовчний міхур'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'розмірами:',
        field: 'ch_z_rozmir'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'з наявністю перегиба:',
        field: 'ch_z_perevib'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'товщина передньої стінки:',
        field: 'ch_z_pered_stinka'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'слиз по задній стінці:',
        field: 'ch_z_sluz_po_zadniy'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Об\'єм жовчного міхура:',
        field: 'ch_z_obyem'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    strong: true,
    items: [
      {
        title: 'Підшлункова залоза:',
        field: 'ch_zaloza'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    strong: true,
    items: [
      {
        title: 'Голівка:',
        field: 'ch_zaloza_golivka'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    strong: true,
    items: [
      {
        title: 'Тіло:',
        field: 'ch_zaloza_tilo'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    strong: true,
    items: [
      {
        title: 'Хвіст:',
        field: 'ch_zaloza_hvist'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    strong: true,
    items: [
      {
        title: 'Селезінка:',
        field: 'ch_zaloza_selezinka'
      }
    ]
  },
  {
    type: 'triple',
    align: 'left',
    strong: true,
    items: [
      {
        title: 'Права нирка:',
        field: 'ch_p_nurka'
      },
      {
        title: 'паренхіма:',
        field: 'ch_p_nurka_parenhima'
      },
      {
        title: 'ЧПК:',
        field: 'ch_p_nurka_chpk'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    strong: true,
    items: [
      {
        title: 'Серединний сегмент:',
        field: 'ch_p_nurka_segment'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    strong: true,
    items: [
      {
        title: 'Порожниста система:',
        field: 'ch_p_nurka_ps'
      }
    ]
  },
  {
    type: 'triple',
    align: 'left',
    strong: true,
    items: [
      {
        title: 'Права нирка:',
        field: 'ch_l_nurka'
      },
      {
        title: 'паренхіма:',
        field: 'ch_l_nurka_parenhima'
      },
      {
        title: 'ЧПК:',
        field: 'ch_l_nurka_chpk'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    strong: true,
    items: [
      {
        title: 'Серединний сегмент:',
        field: 'ch_l_nurka_segment'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    strong: true,
    items: [
      {
        title: 'Порожниста система:',
        field: 'ch_l_nurka_ps'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    strong: true,
    items: [
      {
        title: 'Сечовий міхур:',
        field: 'ch_mihur'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'ЗАКЛЮЧЕННЯ:',
        field: 'ch_results'
      }
    ]
  },
  {
    type: 'single',
    align: 'right margin-10',
    items: [
      {
        title: 'Лікар:',
        field: 'doctors'
      }
    ]
  }
];
