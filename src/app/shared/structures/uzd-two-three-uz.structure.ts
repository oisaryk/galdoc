import { BlankStructureInterface } from '../interfaces/structure';

export const UzdTwoThreeUzStructure: BlankStructureInterface[] = [
  {
    type: 'single',
    align: 'center',
    items: [
      {
        title: '',
        field: 'uz_date'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'П.І.Б:',
        field: 'uz_pib'
      }
    ]
  },
  {
    type: 'double',
    align: 'left',
    items: [
      {
        title: 'Кількість плодів:',
        field: 'uz_plodiv_count'
      },
      {
        title: 'Вага:',
        field: 'uz_vaga'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Положення плода:',
        field: 'uz_poloz_ploda'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Передлежання:',
        field: 'uz_predlezannya'
      }
    ]
  },
  {
    type: 'double',
    align: 'left',
    items: [
      {
        title: 'Серцебиття:',
        field: 'uz_sertce'
      },
      {
        title: 'ЧСС:',
        field: 'uz_chss'
      },
      {
        title: 'Рухи плода:',
        field: 'uz_ruh_ploda'
      }
    ]
  },
  {
    type: 'double',
    align: 'left',
    items: [
      {
        title: 'Біпарієтральний розмір (БПР):',
        field: 'uz_bpr'
      },
      {
        title: 'Структура мозку:',
        field: 'uz_brain_str'
      }
    ]
  },
  {
    type: 'double',
    align: 'left',
    items: [
      {
        title: 'Окружність голови (ОГ):',
        field: 'uz_og'
      },
      {
        title: 'Потиличні роги:',
        field: 'uz_pot_rog'
      }
    ]
  },
  {
    type: 'double',
    align: 'left',
    items: [
      {
        title: 'Лобно-потиличний розмір (ЛПР):',
        field: 'uz_lpr'
      },
      {
        title: 'Бокові шлуночки (БШ):',
        field: 'uz_bsh'
      }
    ]
  },
  {
    type: 'double',
    align: 'left',
    items: [
      {
        title: 'Окружність живота (ОЖ):',
        field: 'uz_ozh'
      },
      {
        title: '4-х камерний розтин серця:',
        field: 'uz_chkam'
      }
    ]
  },
  {
    type: 'double',
    align: 'left',
    items: [
      {
        title: 'Довжина стегна (ДС):',
        field: 'uz_dc'
      },
      {
        title: 'Зріз на трьох судинах:',
        field: 'uz_zriz_tr'
      }
    ]
  },
  {
    type: 'double',
    align: 'left',
    items: [
      {
        title: 'Легенева артерія:',
        field: 'uz_leg_art'
      },
      {
        title: 'Ао:',
        field: 'uz_ao'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Середній діаметр грудної клітини (СДГК):',
        field: 'uz_sdgk'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Носогубний трикутник:',
        field: 'uz_noso_triangle'
      }
    ]
  },
  {
    type: 'double',
    align: 'left',
    items: [
      {
        title: 'Легені (ступінь зрілості):',
        field: 'uz_leg_st'
      },
      {
        title: 'Мозочок:',
        field: 'uz_mozochok'
      }
    ]
  },
  {
    type: 'double',
    align: 'left',
    items: [
      {
        title: 'Кісточка перенісся:',
        field: 'uz_kistochka'
      },
      {
        title: 'К-сть навколоплідних вод:',
        field: 'uz_kilkist_vod'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Амніотичний індекс:',
        field: 'uz_index'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Локалізація плаценти:',
        field: 'uz_platsenty'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Ступінь зрілості:',
        field: 'uz_stupin_zrilosti'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Нижній край плаценти:',
        field: 'uz_nizniy_kray'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Товщина плаценти:',
        field: 'uz_tovshuna'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Патологічні включення плаценти:',
        field: 'uz_patolog_platsenty'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Зрізи петель пуповини в шийномум трикутнику:',
        field: 'uz_zrizu_petel'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Пуповина: кількість судин:',
        field: 'uz_pupovyna'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Пупочне кільке:',
        field: 'uz_pupo_kilce'
      }
    ]
  },
  {
    type: 'double',
    align: 'left',
    items: [
      {
        title: 'Шийка матки:',
        field: 'uz_shuyka'
      },
      {
        title: 'вн/вічко:',
        field: 'uz_vnvichko'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Опис рубця:',
        field: 'uz_rubets'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Цервікальний канал:',
        field: 'uz_tservik'
      }
    ]
  },
  {
    type: 'double',
    align: 'left',
    items: [
      {
        title: 'Термін по гестації:',
        field: 'uz_terminpogest'
      },
      {
        title: 'Термін пологів:',
        field: 'uz_termin'
      }
    ]
  },
  {
    type: 'single',
    align: 'center text-bold margin-10',
    items: [
      {
        title: 'ДОПЛЕРОМЕТРІЯ',
        field: ''
      }
    ]
  },
  {
    type: 'double',
    align: 'left',
    items: [
      {
        title: 'S/D:',
        field: 'uz_sd'
      },
      {
        title: 'Кровоплин судин:',
        field: 'uz_krovoplun'
      }
    ]
  },
  {
    type: 'double',
    align: 'left',
    items: [
      {
        title: 'CMA:',
        field: 'uz_cma'
      },
      {
        title: 'IR:',
        field: 'uz_ir'
      },
      {
        title: 'Pi:',
        field: 'uz_pi'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Висновок УЗД:',
        field: 'uz_result'
      }
    ]
  },
  {
    type: 'single',
    align: 'right margin-10',
    items: [
      {
        title: 'Лікар:',
        field: 'doctors'
      }
    ]
  }
];
