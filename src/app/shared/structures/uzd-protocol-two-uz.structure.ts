import { BlankStructureInterface } from '../interfaces/structure';

export const UzdProtocolTwoUzStructure: BlankStructureInterface[] = [
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: '',
        field: 'two_date'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'П.І.Б:',
        field: 'two_pib'
      }
    ]
  },
  {
    type: 'triple',
    align: 'left',
    items: [
      {
        title: 'OM:',
        field: 'two_om'
      },
      {
        title: 'По гестації:',
        field: 'two_gastatsia'
      },
      {
        title: 'Термін пологів:',
        field: 'two_termin'
      },
      {
        title: 'Вага:',
        field: 'two_vaga'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Кількість плодів:',
        field: 'two_plodiv'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Положення плода:',
        field: 'two_poloz'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Передлежання:',
        field: 'two_pred'
      }
    ]
  },
  {
    type: 'triple',
    align: 'left',
    items: [
      {
        title: 'Серцебиття:',
        field: 'two_sertse'
      },
      {
        title: 'ЧСС:',
        field: 'two_chss'
      },
      {
        title: 'Рухи плода:',
        field: 'two_moves'
      }
    ]
  },
  {
    type: 'full-table',
    align: 'left',
    table_headers: [
      {
        title: 'Параметри плода',
        class: 'left'
      },
      {
        title: 'мм',
        class: 'left'
      },
      {
        title: 'Параметри плода',
        class: 'left'
      },
      {
        title: 'мм',
        class: 'left'
      }
    ],
    table_rows: [
      [
        {
          title: 'Біпарієтальний розмір(БПР)',
          class: 'left'
        },
        {
          field: 'two_bpr'
        },
        {
          title: 'Нирка права',
          class: 'left'
        },
        {
          field: 'two_nurka_right'
        }
      ],
      [
        {
          title: 'Лобно потиличний розмір(ЛПР)',
          class: 'left'
        },
        {
          field: 'two_lpr'
        },
        {
          title: 'Нирка зліва',
          class: 'left'
        },
        {
          field: 'two_nurka_left'
        }
      ],
      [
        {
          title: 'Окружність голови',
          class: 'left'
        },
        {
          field: 'two_head_okr'
        },
        {
          title: 'Сечовий міхур',
          class: 'left'
        },
        {
          field: 'two_setch_mikhur'
        }
      ],
      [
        {
          title: 'Цефалічний індекс',
          class: 'left'
        },
        {
          field: 'two_tsefal_index'
        },
        {
          title: 'Довжини стегна',
          class: 'left'
        },
        {
          field: 'two_dovz_stegna'
        }
      ],
      [
        {
          title: 'Мозочок',
          class: 'left'
        },
        {
          field: 'two_mozochok'
        },
        {
          title: 'Довжина великої бедрової кістки',
          class: 'left'
        },
        {
          field: 'two_vel_bedr_kist'
        }
      ],
      [
        {
          title: 'Інтраокулярний розмір',
          class: 'left'
        },
        {
          field: 'two_intraokul_rozmir'
        },
        {
          title: 'Довжина малої бедрової кістки',
          class: 'left'
        },
        {
          field: 'two_dozv_mal_bedr_kist'
        }
      ],
      [
        {
          title: 'Діаметр грудної клітини (СДГК)',
          class: 'left'
        },
        {
          field: 'two_sdgk'
        },
        {
          title: 'Стопа',
          class: 'left'
        },
        {
          field: 'two_stopa'
        }
      ],
      [
        {
          title: 'Серед. діаметр живота(СДЖ)',
          class: 'left'
        },
        {
          field: 'two_diam_zuv'
        },
        {
          title: 'Довжина плеча',
          class: 'left'
        },
        {
          field: 'two_dovz_plecha'
        }
      ],
      [
        {
          title: 'Окружність живота',
          class: 'left'
        },
        {
          field: 'two_okr_zuvota'
        },
        {
          title: 'Довжина ліктьової кістки',
          class: 'left'
        },
        {
          field: 'two_dovz_likt'
        }
      ],
      [
        {
          title: 'Серце',
          class: 'left'
        },
        {
          field: 'two_sertse_mm'
        },
        {
          title: 'Довжина променевої кістки',
          class: 'left'
        },
        {
          field: 'two_dovz_promenevoi'
        }
      ],
      [
        {
          title: 'Шлунок',
          class: 'left'
        },
        {
          field: 'two_shlunok'
        },
        {
          title: 'Кисть',
          class: 'left'
        },
        {
          field: 'two_kust'
        }
      ]
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Структура мозку',
        field: ''
      }
    ]
  },
  {
    type: 'triple',
    align: 'left',
    items: [
      {
        title: 'Серединне М-ехо:',
        field: 'two_sered_exo'
      },
      {
        title: ',бокові шлуночки мозку:',
        field: 'two_bok_sh_mozku'
      },
      {
        title: ',велика цистерна:',
        field: 'two_big_tsusterna'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Задні рога бокових шлуночків:',
        field: 'two_zadni_roga'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Хребтовий стовбур:',
        field: 'two_stovbur'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Серце:',
        field: 'two_hearth'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Діафрагма:',
        field: 'two_diafragma'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Кишечник:',
        field: 'two_kushechnuk'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Передня черевна стінка:',
        field: 'two_perednya_cherevna'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Структури обличчя. Лицьові розщілини:',
        field: 'two_luts_rozshilunu'
      }
    ]
  },
  {
    type: 'double',
    align: 'left',
    items: [
      {
        title: 'Носова кістка:',
        field: 'two_nosova_kistka'
      },
      {
        title: 'Розмір шийної складки:',
        field: 'two_rozmir_skladku'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Кількість навколоплідних вод:',
        field: 'two_kilkist_vod'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Локалізація плаценти:',
        field: 'two_local_plats'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Нижній край плаценти на',
        field: 'two_nuz_platsenty'
      }
    ]
  },
  {
    type: 'double',
    align: 'left',
    items: [
      {
        title: 'Товщина плаценти:',
        field: 'two_tov_platsenty'
      },
      {
        title: 'Структура плаценти:',
        field: 'two_str_platsenty'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Реверсний кровоток у венозній протоці',
        field: 'two_revers_krovotok'
      }
    ]
  },
  {
    type: 'double',
    align: 'left',
    items: [
      {
        title: 'Пуповина. Кількість судин:',
        field: 'two_kilk_sudun'
      },
      {
        title: 'Обвиття пуповини:',
        field: 'two_obv_pupovunu'
      }
    ]
  },
  {
    type: 'double',
    align: 'left',
    items: [
      {
        title: 'Передлежання судин пуповини:',
        field: 'two_pered_sudun'
      },
      {
        title: 'Оболонкове прикріплення судин:',
        field: 'two_obol_prukrip'
      }
    ]
  },
  {
    type: 'double',
    align: 'left',
    items: [
      {
        title: 'Шийка матки:',
        field: 'two_shuyka_matku'
      },
      {
        title: 'Вн.вічко:',
        field: 'two_vnut_vichko'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Підозра або наявні вади розвитку плода (ВВР):',
        field: 'two_pidozra'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Висновок УЗД:',
        field: 'two_results'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Рекомендації:',
        field: 'two_recomends'
      }
    ]
  },
  {
    type: 'single',
    align: 'right',
    items: [
      {
        title: 'Лікар:',
        field: 'doctors'
      }
    ]
  }
];
