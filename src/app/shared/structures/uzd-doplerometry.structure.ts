import { BlankStructureInterface } from '../interfaces/structure';

export const UzdDoplerometryStructure: BlankStructureInterface[] = [
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: '',
        field: 'dp_date'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'П.І.Б:',
        field: 'dp_pib'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Положення плода:',
        field: 'dp_polozennya'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Опис рубця:',
        field: 'dp_rubets'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Передлежання:',
        field: 'dp_peredlezannya'
      }
    ]
  },
  {
    type: 'double',
    align: 'left',
    items: [
      {
        title: 'Серцебиття:',
        field: 'dp_sertse'
      },
      {
        title: 'ЧСС:',
        field: 'dp_tss'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Рухи плода:',
        field: 'dp_moves'
      }
    ]
  },
  {
    type: 'single',
    align: 'left text-bold',
    items: [
      {
        title: 'Доплерометрія артерії пуповини',
        field: ''
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 's/d:',
        field: 'dp_art_sd'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'IR:',
        field: 'dp_art_ir'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Pi:',
        field: 'dp_art_pi'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Кровоплин:',
        field: 'dp_art_krovo'
      }
    ]
  },
  {
    type: 'single',
    align: 'left text-bold',
    items: [
      {
        title: 'Доплерометрія середньої мозкової артерії',
        field: ''
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 's/d:',
        field: 'dp_mat_sd'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'IR:',
        field: 'dp_mat_ir'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Pi:',
        field: 'dp_mat_pi'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Кровоплин:',
        field: 'dp_mat_krovo'
      }
    ]
  },

  {
    type: 'single',
    align: 'left text-bold',
    items: [
      {
        title: 'Доплерометрія маткових атретій',
        field: ''
      }
    ]
  },
  {
    type: 'single',
    align: 'left text-bold',
    items: [
      {
        title: 'Ліва',
        field: ''
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'IR:',
        field: 'dp_liva_ir'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Pi:',
        field: 'dp_liva_pi'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Кровоплин:',
        field: 'dp_liva_krovo'
      }
    ]
  },
  {
    type: 'single',
    align: 'left text-bold',
    items: [
      {
        title: 'Права',
        field: ''
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'IR:',
        field: 'dp_prava_ir'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Pi:',
        field: 'dp_prava_pi'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Кровоплин:',
        field: 'dp_prava_krovo'
      }
    ]
  },
  {
    type: 'single',
    align: 'right',
    items: [
      {
        title: 'Лікар:',
        field: 'doctors'
      }
    ]
  }
];
