import { BlankStructureInterface } from '../interfaces/structure';

export const UzdShitovidnaStructure: BlankStructureInterface[] = [
  {
    type: 'single',
    align: 'center',
    items: [
      {
        title: '',
        field: 'sh_date'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'П.І.Б:',
        field: 'sh_pib'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Вік:',
        field: 'sh_vik'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Щитовидна залоза розташована:',
        field: 'sh_roztash'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Капсула:',
        field: 'sh_capsula'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Об\'єм правої частки:',
        field: 'sh_p_chastka'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Ехогенність:',
        field: 'sh_p_eho'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Структура:',
        field: 'sh_p_strycture'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Об\'єм лівої частки:',
        field: 'sh_l_chastka'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Ехогенність:',
        field: 'sh_l_eho'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Структура:',
        field: 'sh_l_strycture'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Перешийок:',
        field: 'sh_pereshuyok'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Загальний об\'єм залози:',
        field: 'sh_zagal_ob'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Висновок:',
        field: 'sh_results'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Рекомендовано:',
        field: 'sh_recomends'
      }
    ]
  },
  {
    type: 'single',
    align: 'right margin-10',
    items: [
      {
        title: 'Лікар:',
        field: 'doctors'
      }
    ]
  }
];
