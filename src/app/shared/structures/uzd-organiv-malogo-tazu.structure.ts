import { BlankStructureInterface } from '../interfaces/structure';

export const UzdOrganivMalogoTazuStructure: BlankStructureInterface[] = [
  {
    type: 'double',
    items: [
      {
        title: '',
        field: 'omt_date'
      },
      {
        title: '',
        field: 'omt_type'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'День менструального циклу:',
        field: 'omt_menstrual'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'П.І.Б',
        field: 'omt_pib'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'МАТКА:',
        field: 'omt_matka'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Положення:',
        field: 'omt_polozennya'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Розміри тіла матки:',
        field: 'omt_rozmir'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Ехоструктура:',
        field: 'omt_exostructure'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Передня стінка:',
        field: 'omt_perednya_stinka'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Задня стінка:',
        field: 'omt_zadnya_stinka'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Дно:',
        field: 'omt_dno'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'ендометрій:',
        field: 'omt_endometrii'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'порожнина матки:',
        field: 'omt_poroznyna'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'шийка матки:',
        field: 'omt_shuyka'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'цервікальний канал:',
        field: 'omt_tservikalnyy_kanal'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'правий ЯЄЧНИК:',
        field: 'omt_right_yayechnyk'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Розташування:',
        field: 'omt_right_rozstashuvannya'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'розміром:',
        field: 'omt_rigth_rozmir'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'в структурі яйника:',
        field: 'omt_rigth_v_structuri'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'права маткова труба:',
        field: 'omt_right_truba'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'лівий ЯЄЧНИК:',
        field: 'omt_left_yayechnyk'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Розташування:',
        field: 'omt_left_rozstashuvannya'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'розміром:',
        field: 'omt_left_rozmir'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'в структурі яйника:',
        field: 'omt_left_v_structuri'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'ліва маткова труба:',
        field: 'omt_left_truba'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Рідина в дугласовому просторі:',
        field: 'omt_riduna_v_duglas'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'ЗАКЛЮЧЕННЯ:',
        field: 'omt_zakluchennya'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'РЕКОМЕНДОВАНО:',
        field: 'omt_rekomendatsii'
      }
    ]
  },
  {
    type: 'single',
    align: 'center',
    items: [
      {
        title: 'Лікар:',
        field: 'doctors'
      }
    ]
  }
];
