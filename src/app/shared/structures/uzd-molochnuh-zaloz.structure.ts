import { BlankStructureInterface } from '../interfaces/structure';

export const UzdOrganivMalogoTazuStructure: BlankStructureInterface[] = [
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: '',
        field: 'mol_date'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'П.І.Б',
        field: 'mol_pib'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'День менструального циклу',
        field: 'mol_menstrual_day'
      }
    ]
  },
  {
    type: 'single',
    align: 'center text-bold',
    items: [
      {
        title: 'Права молочна залоза',
        field: ''
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Переважає:',
        field: 'mol_right_perevazae'
      }
    ]
  },
  {
    type: 'single',
    align: 'left text-bold',
    items: [
      {
        title: 'Верхній зовнішній квадрант',
        field: ''
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'а) анехогенні вкл.:',
        field: 'mol_pvzk_aneho'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'б) гіпоехогенні:',
        field: 'mol_pvzk_gipo'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'в) гетерогенні вкл.:',
        field: 'mol_pvvk_aneho'
      }
    ]
  },
  {
    type: 'single',
    align: 'left text-bold',
    items: [
      {
        title: 'Верхній внутрішній квадрант',
        field: ''
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'а) анехогенні вкл.:',
        field: 'mol_pvvk_aneho'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'б) гіпоехогенні:',
        field: 'mol_pvvk_gipo'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'в) гетерогенні вкл.:',
        field: 'mol_pvvk_getero'
      }
    ]
  },
  {
    type: 'single',
    align: 'left text-bold',
    items: [
      {
        title: 'Нижній зовнішній квадрант',
        field: ''
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'а) анехогенні вкл.:',
        field: 'mol_pnzk_aneho'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'б) гіпоехогенні:',
        field: 'mol_pnzk_gipo'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'в) гетерогенні вкл.:',
        field: 'mol_pnzk_getero'
      }
    ]
  },
  {
    type: 'single',
    align: 'left text-bold',
    items: [
      {
        title: 'Нижній внутрішній квадрант',
        field: ''
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'а) анехогенні вкл.:',
        field: 'mol_pnvk_aneho'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'б) гіпоехогенні:',
        field: 'mol_pnvk_gipo'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'в) гетерогенні вкл.:',
        field: 'mol_pnvk_getero'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Ділянка соска:',
        field: 'mol_plds'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Молочні ходи:',
        field: 'mol_plmh'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Регіональні лімфовузли:',
        field: 'mol_prlv'
      }
    ]
  },
  {
    type: 'single',
    align: 'center text-bold',
    items: [
      {
        title: 'Ліва молочна залоза',
        field: ''
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Переважає:',
        field: 'mol_left_perevazae'
      }
    ]
  },
  {
    type: 'single',
    align: 'left text-bold',
    items: [
      {
        title: 'Верхній зовнішній квадрант',
        field: ''
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'а) анехогенні вкл.:',
        field: 'mol_lvzk_aneho'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'б) гіпоехогенні:',
        field: 'mol_lvzk_gipo'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'в) гетерогенні вкл.:',
        field: 'mol_lvzk_getero'
      }
    ]
  },
  {
    type: 'single',
    align: 'left text-bold',
    items: [
      {
        title: 'Верхній внутрішній квадрант',
        field: ''
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'а) анехогенні вкл.:',
        field: 'mol_lvvk_aneho'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'б) гіпоехогенні:',
        field: 'mol_lvvk_gipo'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'в) гетерогенні вкл.:',
        field: 'mol_lvvk_getero'
      }
    ]
  },
  {
    type: 'single',
    align: 'left text-bold',
    items: [
      {
        title: 'Нижній зовнішній квадрант',
        field: ''
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'а) анехогенні вкл.:',
        field: 'mol_lnzk_aneho'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'б) гіпоехогенні:',
        field: 'mol_lnzk_gipo'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'в) гетерогенні вкл.:',
        field: 'mol_lnzk_getero'
      }
    ]
  },
  {
    type: 'single',
    align: 'left text-bold',
    items: [
      {
        title: 'Нижній внутрішній квадрант',
        field: ''
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'а) анехогенні вкл.:',
        field: 'mol_lnvk_aneho'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'б) гіпоехогенні:',
        field: 'mol_lnvk_gipo'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'в) гетерогенні вкл.:',
        field: 'mol_lnvk_getero'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Ділянка соска:',
        field: 'mol_llds'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Молочні ходи:',
        field: 'mol_llmh'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Регіональні лімфовузли:',
        field: 'mol_lrlv'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'ЗАКЛЮЧЕННЯ:',
        field: 'mol_zakluchennya'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'РЕКОМЕНДОВАНО:',
        field: 'mol_recomendovano'
      }
    ]
  },
  {
    type: 'single',
    align: 'right',
    items: [
      {
        title: 'Лікар:',
        field: 'doctors'
      }
    ]
  }
];
