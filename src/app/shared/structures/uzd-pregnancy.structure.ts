import { BlankStructureInterface } from '../interfaces/structure';

export const UzdPregnancyStructure: BlankStructureInterface[] = [
  {
    type: 'double',
    items: [
      {
        title: '',
        field: 'p_date'
      },
      {
        title: '',
        field: 'p_type'
      }
    ]
  },
  {
    type: 'double',
    items: [
      {
        title: 'Перший день ОМ:',
        field: 'p_first_day_om'
      },
      {
        title: 'Термін пологів:',
        field: 'p_termin_pologiv'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'П.І.Б',
        field: 'p_pib'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'МАТКА',
        field: 'p_matka'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Положення:',
        field: 'p_polozennya'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Розміри тіла матки:',
        field: 'p_rozmir'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Ехоструктура:',
        field: 'p_exostructure'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'В порожнині матки плідне яйце:',
        field: 'p_plivne'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Куприко-тім\'яний розмір (КТР):',
        field: 'p_ktr'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Серцебиття:',
        field: 'p_sertse'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Жовточний мішок:',
        field: 'p_zhovtochnyy_mishok'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Ділянки відшарування плідного яйця:',
        field: 'p_dilyanku'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Тонус міометрію:',
        field: 'p_tonus'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Шийка матки:',
        field: 'p_shuyka'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'вн.вічко:',
        field: 'p_vichko'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Правий ЯЄЧНИК ехоструктура:',
        field: 'p_p_yaye_exostructure'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Лівий ЯЄЧНИК ехоструктура:',
        field: 'p_l_yaye_exostructure'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Рідина в дугласовому просторі:',
        field: 'p_riduna_v_duglas'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'По гестації:',
        field: 'p_po_gastatsii'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'ЗАКЛЮЧЕННЯ:',
        field: 'p_zakluchennya'
      }
    ]
  },
  {
    type: 'single',
    align: 'center',
    items: [
      {
        title: 'Лікар:',
        field: 'doctors'
      }
    ]
  }
];
