import { BlankStructureInterface } from '../interfaces/structure';

export const UzdTservikometriaStructure: BlankStructureInterface[] = [
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: '',
        field: 'ts_date'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'П.І.Б:',
        field: 'ts_pib'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Шийка матки:',
        field: 'ts_shuyka'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Цервікальний канал:',
        field: 'ts_canal'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Внутрішнє вічко:',
        field: 'ts_vichko'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Серцебиття:',
        field: 'ts_sertse'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'ЧСС:',
        field: 'ts_chss'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Рухи плода:',
        field: 'ts_moves'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Висновок:',
        field: 'ts_vusnovok'
      }
    ]
  },
  {
    type: 'single',
    align: 'right',
    items: [
      {
        title: 'Лікар:',
        field: 'doctors'
      }
    ]
  }
];
