import { BlankStructureInterface } from '../interfaces/structure';

export const UzdProtocolOneUzStructure: BlankStructureInterface[] = [
  {
    type: 'double',
    align: 'left',
    items: [
      {
        title: '',
        field: 'one_date'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'П.І.Б:',
        field: 'one_pib'
      }
    ]
  },
  {
    type: 'triple',
    align: 'left',
    items: [
      {
        title: 'ОМ:',
        field: 'one_om'
      },
      {
        title: 'По гестації:',
        field: 'one_gastatsia'
      },
      {
        title: 'Термін пологів:',
        field: 'one_termin'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Методика сканування:',
        field: 'one_method'
      }
    ]
  },
  {
    type: 'triple',
    align: 'left',
    items: [
      {
        title: 'Розміри матки: довжина',
        field: 'one_dovz'
      },
      {
        title: 'ширина:',
        field: 'one_shuruna'
      },
      {
        title: 'передньо-задній',
        field: 'one_pered_zad'
      }
    ]
  },
  {
    type: 'double',
    align: 'left',
    items: [
      {
        title: 'Структура біометрію:',
        field: 'one_structura_biomentiyu'
      },
      {
        title: 'Тонус біометрію:',
        field: 'one_tonus'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'локальний гіпертонус:',
        field: 'one_local_hipertonus'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'У порожнині матки візуалізується:',
        field: 'one_u_poroz_vizial'
      }
    ]
  },
  {
    type: 'double',
    align: 'left',
    items: [
      {
        title: 'Діаметр плідного яйця:',
        field: 'one_diamert_egg'
      },
      {
        title: 'Кількість ембріонів:',
        field: 'one_kilkist_embrioniv'
      },
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Ділянки відшарування плідного яйця:',
        field: 'one_vidsharuvannya'
      }
    ]
  },
  {
    type: 'title',
    align: 'center',
    items: [
      {
        title: 'БІОМЕТРІЯ ПЛОДУ'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Куприко-тім\'яний розмір (КТР):',
        field: 'one_kupriko'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Біпарієтальний розмір(БПР):',
        field: 'one_biparietal'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Кістки черепа:',
        field: 'one_skull_bones'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Окружність живота:',
        field: 'one_okruznist'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Середній діаметр живота:',
        field: 'one_ser_diametr'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Довжина стегна:',
        field: 'one_dov_stegna'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Комірцевий простір:',
        field: 'one_komirts_prostir'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Наявність носової кістки:',
        field: 'one_nose_kist'
      }
    ]
  },
  {
    type: 'triple',
    align: 'left',
    items: [
      {
        title: 'Серце:',
        field: 'one_hearth'
      },
      {
        title: 'Серцебиття:',
        field: 'one_hearth_bom'
      },
      {
        title: 'ЧСС:',
        field: 'one_chss'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Передня черевна стінка:',
        field: 'one_pered_cherevna'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Рухова активність плода:',
        field: 'one_ruh_ploda'
      }
    ]
  },
  {
    type: 'triple',
    align: 'left',
    items: [
      {
        title: 'Локалізація хоріону:',
        field: 'one_local_horionu'
      },
      {
        title: 'товщина:',
        field: 'one_tovshuna'
      },
      {
        title: 'структура:',
        field: 'one_structura'
      }
    ]
  },
  {
    type: 'triple',
    align: 'left',
    items: [
      {
        title: 'Шийка матки:',
        field: 'one_shuyka'
      },
      {
        title: 'Цервікальний канал:',
        field: 'one_tserv_canal'
      },
      {
        title: 'Діаметр внутрішнього вічка:',
        field: 'one_diam_vnutrishnogo'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Стан придатків матки:',
        field: 'one_stan_prudatkiv'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Особливості:',
        field: 'one_osobluvosti'
      }
    ]
  },
  {
    type: 'single',
    align: 'left',
    items: [
      {
        title: 'Висновок УЗД:',
        field: 'one_results'
      }
    ]
  },
  {
    type: 'single',
    align: 'right',
    items: [
      {
        title: 'Лікар:',
        field: 'doctors'
      }
    ]
  }
];
