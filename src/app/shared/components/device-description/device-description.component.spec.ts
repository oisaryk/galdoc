import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeviceDescriptionComponent } from './device-description.component';

describe('DeviceDescriptionComponent', () => {
  let component: DeviceDescriptionComponent;
  let fixture: ComponentFixture<DeviceDescriptionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeviceDescriptionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeviceDescriptionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
