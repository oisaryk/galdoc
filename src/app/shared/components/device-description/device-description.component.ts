import { Component, Input, OnInit } from '@angular/core';
import { PrintDataInterface } from '../../interfaces/print-data.interface';

@Component({
  selector: 'app-device-description',
  templateUrl: './device-description.component.html',
  styleUrls: ['./device-description.component.scss']
})
export class DeviceDescriptionComponent implements OnInit {

  @Input() deviceSize: PrintDataInterface;

  constructor() { }

  ngOnInit(): void {
  }

}
