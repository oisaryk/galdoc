import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PrintDataInterface } from '../../interfaces/print-data.interface';

@Component({
  selector: 'app-print-actions',
  templateUrl: './print-actions.component.html',
  styleUrls: ['./print-actions.component.scss']
})
export class PrintActionsComponent implements OnInit {

  showPrintActions = true;
  showSettings = false;
  inDesignMode = false;

  @Input() options: PrintDataInterface;

  @Output() print = new EventEmitter();
  @Output() backAndEdit = new EventEmitter();
  @Output() backAndAdd = new EventEmitter();
  @Output() optionsUpdate = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  printPage() {
    this.print.emit();
  }

  doBackAndEdit() {
    this.backAndEdit.emit();
  }

  doBackAndAdd() {
    this.backAndAdd.emit();
  }

  updateOptions(type: string) {
    switch (type) {
      case 'increase-logo-size':
        this.options.logoSize += 10;
        this.optionsUpdate.emit(this.options);
        break;
      case 'decrease-logo-size':
        this.options.logoSize -= 10;
        this.optionsUpdate.emit(this.options);
        break;
      case 'increase-title-size':
        this.options.titleSize ++;
        this.optionsUpdate.emit(this.options);
        break;
      case 'decrease-title-size':
        this.options.titleSize --;
        this.optionsUpdate.emit(this.options);
        break;
      case 'increase-device-size':
        this.options.deviceSize ++;
        this.optionsUpdate.emit(this.options);
        break;
      case 'decrease-device-size':
        this.options.deviceSize --;
        this.optionsUpdate.emit(this.options);
        break;
      case 'increase-font-size':
        this.options.fontSize ++;
        this.optionsUpdate.emit(this.options);
        break;
      case 'decrease-font-size':
        this.options.fontSize --;
        this.optionsUpdate.emit(this.options);
        break;
      case 'increase-line-height':
        this.options.lineHeight += 0.1;
        this.optionsUpdate.emit(this.options);
        break;
      case 'decrease-line-height':
        this.options.lineHeight -= 0.1;
        this.optionsUpdate.emit(this.options);
        break;
      case 'toggle-strong-titles':
        this.options.strongTitles = !this.options.strongTitles;
        this.optionsUpdate.emit(this.options);
        break;
      case 'toggle-border-in-value':
        this.options.borderInValues = !this.options.borderInValues;
        this.optionsUpdate.emit(this.options);
        break;
      case 'toggle-empty-rows':
        this.options.showEmptyRows = !this.options.showEmptyRows;
        this.optionsUpdate.emit(this.options);
        break;
      default:
        break;
    }
  }

  toggleDesignMode() {
    const currentMode = document.designMode;
    if (currentMode === 'off') {
      document.designMode = 'on';
      this.inDesignMode = true;
    } else {
      document.designMode = 'off';
      this.inDesignMode = false;
    }
  }

  toggleSettings() {
    this.showSettings = !this.showSettings;
  }

}
