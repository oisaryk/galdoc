import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrintActionsComponent } from './print-actions.component';

describe('PrintActionsComponent', () => {
  let component: PrintActionsComponent;
  let fixture: ComponentFixture<PrintActionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrintActionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrintActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
