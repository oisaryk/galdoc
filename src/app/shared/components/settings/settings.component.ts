import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  form: FormGroup;

  doctors = ['Іванова Ю.В', 'Боднар М.П'];

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.initForm();
  }

  initForm() {
    this.form = this.fb.group({
      name: ['']
    });

    const localStorageItem = localStorage.getItem('doctors');

    if (localStorageItem) {
      this.doctors = localStorageItem.split('|');
    } else {
      localStorage.setItem('doctors', this.doctors.join('|'));
    }
  }

  submit() {
    const values = this.form.value;

    if (values.name) {
      this.doctors.push(values.name);
    }

    localStorage.setItem('doctors', this.doctors.join('|'));

    this.form.reset();
  }

  removeDoctor(i) {
    this.doctors.splice(i, 1);

    localStorage.setItem('doctors', this.doctors.join('|'));
  }

}
