import { Component, Input, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { isEmpty } from 'lodash';

@Component({
  selector: 'app-doctors',
  templateUrl: './doctors.component.html',
  styleUrls: ['./doctors.component.scss']
})
export class DoctorsComponent implements OnInit {

  @Input() parentForm: FormGroup;
  @Input() fontSize: number;

  initList = ['Іванова Ю.В', 'Боднар М.П'];

  doctors;

  ngOnInit() {
    const store = localStorage.getItem('doctors');

    if (store && !isEmpty(store)) {
      this.doctors = store.split('|') || this.initList;
    } else {
      this.doctors = this.initList;
      localStorage.setItem('doctors', this.initList.join('|'));
    }
  }

}
