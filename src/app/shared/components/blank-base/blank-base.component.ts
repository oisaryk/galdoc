import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DateService } from '../../services/date.service';
import { PrintDataService } from '../../services/print-data.service';
import { PrintDataInterface } from '../../interfaces/print-data.interface';

@Component({
  template: '',
  selector: 'app-blank-base'
})
export class BlankBaseComponent implements OnInit, OnDestroy {

  form: FormGroup;
  currentDate: string;
  options: PrintDataInterface;

  constructor(
    public fb: FormBuilder,
    public date: DateService,
    public data: PrintDataService
  ) {}

  ngOnInit() {
    this.currentDate = this.date.getCurrentUaDate();
  }

  copyDataToTextArea(text, controls) {
    const currentValue = this.form.controls[controls].value;
    this.form.patchValue({
      [controls]: currentValue ? `${currentValue}\n${text}` : text
    });
  }

  updateFontSize(size: number) {
    this.options.fontSize = size;
  }

  ngOnDestroy() {
  }

}
