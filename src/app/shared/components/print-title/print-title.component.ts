import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-print-title',
  templateUrl: './print-title.component.html',
  styleUrls: ['./print-title.component.scss']
})
export class PrintTitleComponent implements OnInit {

  @Input() titleSize;
  @Input() title;

  constructor() { }

  ngOnInit(): void {
  }

}
