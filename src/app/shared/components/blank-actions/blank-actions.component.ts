import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-blank-actions',
  templateUrl: './blank-actions.component.html',
  styleUrls: ['./blank-actions.component.scss']
})
export class BlankActionsComponent implements OnInit {

  @Input() font: number;
  @Output() fontSize = new EventEmitter();
  @Output() print = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  increaseFontSize() {
    if (this.font !== 24) {
      this.font ++;
      this.fontSize.emit(this.font);
    }
  }

  decreaseFontSize() {
    if (this.font !== 12) {
      this.font --;
      this.fontSize.emit(this.font);
    }
  }

  printPage() {
    this.print.emit();
  }

}
