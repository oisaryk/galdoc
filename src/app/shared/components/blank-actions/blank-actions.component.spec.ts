import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlankActionsComponent } from './blank-actions.component';

describe('BlankActionsComponent', () => {
  let component: BlankActionsComponent;
  let fixture: ComponentFixture<BlankActionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlankActionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlankActionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
