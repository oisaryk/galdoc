import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { DateService } from '../../services/date.service';
import { PrintDataService } from '../../services/print-data.service';
import { PrintDataInterface } from '../../interfaces/print-data.interface';
import { Router } from '@angular/router';
import { isEmpty } from 'lodash';

@Component({
  template: '',
  selector: 'app-blank-base'
})
export class PrintBaseComponent implements OnInit, OnDestroy {

  form: FormGroup;
  options: PrintDataInterface;
  fontSizeValue: number;

  showActions = true;

  constructor(
    public fb: FormBuilder,
    public date: DateService,
    public data: PrintDataService,
    private router: Router
  ) {}

  ngOnInit() {

    this.options = {
      logoSize: 75,
      titleSize: 16,
      deviceSize: 11,
      fontSize: 12,
      lineHeight: 1.1
    };

    this.data.blankData.subscribe(values => {
      if (values && !isEmpty(values.data)) {
        this.options = {
          ...this.options,
          ...values
        };
      } else {
        this.router.navigateByUrl('/');
      }
    });
  }

  printCurrentBlank() {
    this.showActions = false;
    this.windowPrint();
  }

  navigateBackAndEdit() {
    const route = this.options.route ? this.options.route : './';
    this.router.navigateByUrl(route);
  }

  navigateBackAndAdd() {
    const route = this.options.route ? this.options.route : './';
    this.router.navigateByUrl(route);
    this.data.updateData({});
  }

  onOptionsUpdate(options: PrintDataInterface) {
    this.fontSizeValue = options.fontSize;
  }

  windowPrint() {
    setTimeout(() => {
      window.print();
      this.showActions = true;
    }, 0);
  }

  ngOnDestroy() {
  }

}
