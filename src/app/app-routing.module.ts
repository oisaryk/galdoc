import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BlankLayoutComponent } from './layout/blank-layout/blank-layout.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { SettingsComponent } from './shared/components/settings/settings.component';
import { UzdOrganivMalogoTazuComponent } from './pages/blanks/uzd-organiv-malogo-tazu/uzd-organiv-malogo-tazu.component';
import { PrintLayoutComponent } from './layout/print-layout/print-layout.component';
import { UzdPregnancyComponent } from './pages/blanks/uzd-pregnancy/uzd-pregnancy.component';
import { UzdMolochnuhZalozComponent } from './pages/blanks/uzd-molochnuh-zaloz/uzd-molochnuh-zaloz.component';
import { UzdProtocolComponent } from './pages/blanks/uzd-protocol/uzd-protocol.component';
import { UzdTservikometriaComponent } from './pages/blanks/uzd-tservikometria/uzd-tservikometria.component';
import { UzdDoplerometryComponent } from './pages/blanks/uzd-doplerometry/uzd-doplerometry.component';
import { UzdTwoThreeUzComponent } from './pages/blanks/uzd-two-three-uz/uzd-two-three-uz.component';
import { UzdProtocolTwoUzComponent } from './pages/blanks/uzd-protocol-two-uz/uzd-protocol-two-uz.component';
import { UzdProtocolOneUzComponent } from './pages/blanks/uzd-protocol-one-uz/uzd-protocol-one-uz.component';
import { UzdOrganivCherevnoiComponent } from './pages/blanks/uzd-organiv-cherevnoi/uzd-organiv-cherevnoi.component';
import { UzdShutovidnaComponent } from './pages/blanks/uzd-shutovidna/uzd-shutovidna.component';
import { UzdStatevaComponent } from './pages/blanks/uzd-stateva/uzd-stateva.component';


const routes: Routes = [
  {
    path: 'blank',
    component: BlankLayoutComponent,
    children: [
      {
        path: 'dashboard',
        component: DashboardComponent
      },
      {
        path: 'settings',
        component: SettingsComponent
      },
      {
        path: 'uzd-organiv-malogo-tazu',
        component: UzdOrganivMalogoTazuComponent
      },
      {
        path: 'uzd-pregnancy',
        component: UzdPregnancyComponent
      },
      {
        path: 'uzd-molochnuh-zaloz',
        component: UzdMolochnuhZalozComponent
      },
      {
        path: 'uzd-protocol',
        component: UzdProtocolComponent
      },
      {
        path: 'uzd-tservikometria',
        component: UzdTservikometriaComponent
      },
      {
        path: 'uzd-doplerometry',
        component: UzdDoplerometryComponent
      },
      {
        path: 'uzd-two-three-uz',
        component: UzdTwoThreeUzComponent
      },
      {
        path: 'uzd-protocol-two-uz',
        component: UzdProtocolTwoUzComponent
      },
      {
        path: 'uzd-protocol-one-uz',
        component: UzdProtocolOneUzComponent
      },
      {
        path: 'uzd-organiv-cherevnoi',
        component: UzdOrganivCherevnoiComponent
      },
      {
        path: 'uzd-shitovidna',
        component: UzdShutovidnaComponent
      },
      {
        path: 'uzd-stateva',
        component: UzdStatevaComponent
      }
    ]
  },
  {
    path: 'print',
    component: PrintLayoutComponent
  },
  {
    path: '**',
    redirectTo: 'blank/dashboard',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
