import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ClarityModule } from '@clr/angular';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './layout/header/header.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { BlankLayoutComponent } from './layout/blank-layout/blank-layout.component';
import { DoctorsComponent } from './shared/components/doctors/doctors.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SettingsComponent } from './shared/components/settings/settings.component';
import { BlankBaseComponent } from './shared/components/blank-base/blank-base.component';
import { DateService } from './shared/services/date.service';
import { PrintDataService } from './shared/services/print-data.service';
import { UzdOrganivMalogoTazuComponent } from './pages/blanks/uzd-organiv-malogo-tazu/uzd-organiv-malogo-tazu.component';
import { DatePipe } from '@angular/common';
import { BlankActionsComponent } from './shared/components/blank-actions/blank-actions.component';
import { LogoComponent } from './shared/components/logo/logo.component';
import { DeviceDescriptionComponent } from './shared/components/device-description/device-description.component';
import { PrintTitleComponent } from './shared/components/print-title/print-title.component';
import { PrintActionsComponent } from './shared/components/print-actions/print-actions.component';
import { PrintLayoutComponent } from './layout/print-layout/print-layout.component';
import { PrintBaseComponent } from './shared/components/print-base/print-base.component';
import { SimpleBlankComponent } from './pages/print/simple-blank/simple-blank.component';
import { RouterExtentionService } from './shared/services/router-extention.service';
import { RouterModule } from '@angular/router';
import { UzdPregnancyComponent } from './pages/blanks/uzd-pregnancy/uzd-pregnancy.component';
import { UzdMolochnuhZalozComponent } from './pages/blanks/uzd-molochnuh-zaloz/uzd-molochnuh-zaloz.component';
import { UzdProtocolComponent } from './pages/blanks/uzd-protocol/uzd-protocol.component';
import { UzdTservikometriaComponent } from './pages/blanks/uzd-tservikometria/uzd-tservikometria.component';
import { UzdDoplerometryComponent } from './pages/blanks/uzd-doplerometry/uzd-doplerometry.component';
import { UzdTwoThreeUzComponent } from './pages/blanks/uzd-two-three-uz/uzd-two-three-uz.component';
import { UzdProtocolTwoUzComponent } from './pages/blanks/uzd-protocol-two-uz/uzd-protocol-two-uz.component';
import { UzdProtocolOneUzComponent } from './pages/blanks/uzd-protocol-one-uz/uzd-protocol-one-uz.component';
import { UzdOrganivCherevnoiComponent } from './pages/blanks/uzd-organiv-cherevnoi/uzd-organiv-cherevnoi.component';
import { UzdShutovidnaComponent } from './pages/blanks/uzd-shutovidna/uzd-shutovidna.component';
import { UzdStatevaComponent } from './pages/blanks/uzd-stateva/uzd-stateva.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DashboardComponent,
    BlankLayoutComponent,
    DoctorsComponent,
    SettingsComponent,
    BlankBaseComponent,
    UzdOrganivMalogoTazuComponent,
    BlankActionsComponent,
    LogoComponent,
    DeviceDescriptionComponent,
    PrintTitleComponent,
    PrintActionsComponent,
    PrintLayoutComponent,
    PrintBaseComponent,
    SimpleBlankComponent,
    UzdPregnancyComponent,
    UzdMolochnuhZalozComponent,
    UzdProtocolComponent,
    UzdTservikometriaComponent,
    UzdDoplerometryComponent,
    UzdTwoThreeUzComponent,
    UzdProtocolTwoUzComponent,
    UzdProtocolOneUzComponent,
    UzdOrganivCherevnoiComponent,
    UzdShutovidnaComponent,
    UzdStatevaComponent,
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    ClarityModule,
    BrowserAnimationsModule,
    RouterModule
  ],
  exports: [
    BlankBaseComponent,
    PrintBaseComponent
  ],
  providers: [
    DatePipe,
    DateService,
    PrintDataService,
    RouterExtentionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
